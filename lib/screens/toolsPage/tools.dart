import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ToolsPage extends StatelessWidget {
  const ToolsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff253334),
      body: Scaffold(
        backgroundColor: const Color(0xff253334),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset('assets/Home/MenuIcon.svg'),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: SvgPicture.asset(
                        'assets/Home/Logo.svg',
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        image: const DecorationImage(
                            image: AssetImage('assets/Home/Avatar.JPG'),
                            fit: BoxFit.cover),
                        // color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 80,
                ),
                const Text(
                  'Tools',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 35),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    //Mood Jurnal
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 50, 0, 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset('assets/Home/Moodj.png'),
                            Text(
                              'Mood Journal',
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      width: 153,
                      height: 115,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/Home/Mood Journal.png'),
                            fit: BoxFit.cover),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    //Mood Buster
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 50, 0, 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset('assets/Home/Mood.png'),
                            Text(
                              'Mood Booster',
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      width: 153,
                      height: 115,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/Home/Mood Booster.png'),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    //Ppsitive Notes
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 50, 0, 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset('assets/Home/Positive.png'),
                            Text(
                              'Positive Notes',
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      width: 153,
                      height: 115,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/Home/Positive Notes.png'),
                            fit: BoxFit.cover),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    //Triggle Plan
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(15, 50, 0, 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Image.asset('assets/Home/Trigger.png'),
                            Text(
                              'Trigger Plan',
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                      width: 153,
                      height: 115,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/Home/Trigger Plan.png'),
                            fit: BoxFit.cover),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    //Goal Trainer
                    Container(
                      child: Padding(
                        padding:
                            const EdgeInsets.only(left: 15, right: 15, top: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [Image.asset('assets/Home/Goal2.png')],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Image.asset('assets/Home/Goal.png'),
                                Text(
                                  'Goal Trainer',
                                  style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      width: 153,
                      height: 115,
                      decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/Home/Goal Trainer.png'),
                            fit: BoxFit.cover),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
