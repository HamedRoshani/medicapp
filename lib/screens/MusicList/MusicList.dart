import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medicapp/data/data.dart';
import 'package:medicapp/exeptions/intexeptions.dart';

class MusicList extends StatelessWidget {
  final List<ListItems> data;
  const MusicList({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff253334),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset('assets/Home/MenuIcon.svg'),
                  Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: SvgPicture.asset(
                      'assets/Home/Logo.svg',
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      image: const DecorationImage(
                          image: AssetImage('assets/Home/Avatar.JPG'),
                          fit: BoxFit.cover),
                      // color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Stack(
                children: [
                  Container(
                    height: 195,
                    width: 339,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      image: const DecorationImage(
                        image: AssetImage('assets/Home/RelaxSounds.png'),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 35),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'Relax Sounds ',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: '',
                                  fontSize: 22),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            const SizedBox(
                              width: 189,
                              child: Text(
                                'Sometimes the most productive thing you can do is relax.',
                                style: TextStyle(
                                    fontFamily: '',
                                    color: Colors.white,
                                    fontSize: 12),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: 138,
                              height: 39,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    Text(
                                      'play now',
                                      style: TextStyle(
                                          fontFamily: '',
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                    SizedBox(
                                      width: 6,
                                    ),
                                    CircleAvatar(
                                        radius: 8,
                                        backgroundColor: Colors.black,
                                        child: Center(
                                          child: Icon(
                                            CupertinoIcons.play_fill,
                                            color: Colors.white,
                                            size: 9,
                                          ),
                                        ))
                                  ]),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(10, 0, 10, 80),
                  child: ListView.builder(
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      final dataitems = data[index];
                      return Padding(
                        padding: const EdgeInsets.only(top: 25),
                        child: Container(
                          width: 299,
                          height: 65,
                          // color: Colors.amber,
                          child: Row(
                            children: [
                              Image.asset(dataitems.image),
                              const SizedBox(
                                width: 30,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    dataitems.name,
                                    style: const TextStyle(
                                        fontFamily: '',
                                        color: Colors.white,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    dataitems.listeningCount.listendCount,
                                    style: const TextStyle(
                                        fontFamily: '', fontSize: 12),
                                  ),
                                ],
                              ),
                              Expanded(
                                child: Center(
                                  child: Text(
                                    dataitems.time,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
