import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medicapp/test/chart.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

int pageIndex = 0;

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff253334),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 50,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset('assets/Home/MenuIcon.svg'),
                  Padding(
                    padding: const EdgeInsets.only(left: 0),
                    child: SvgPicture.asset(
                      'assets/Home/Logo.svg',
                    ),
                  ),
                  const Text(
                    'edit',
                    style: TextStyle(
                        fontFamily: '',
                        fontSize: 15,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            CircleAvatar(
              backgroundImage: AssetImage('assets/Home/Avatar.JPG'),
              radius: 100,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              'Hamed Roshani',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 30,
                  fontWeight: FontWeight.bold),
            ),
            Text('Sari,Iran'),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                InkWell(
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: (() {
                    setState(() {
                      pageIndex = 0;
                    });
                  }),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'STATS',
                        style: TextStyle(
                            fontSize: 16,
                            color: pageIndex == 0
                                ? Colors.white
                                : Color(0xff3A5051),
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        height: 4,
                        width: MediaQuery.of(context).size.width / 2,
                        decoration: BoxDecoration(
                            color: pageIndex == 0
                                ? Color(0xff95CBCF)
                                : Color(0xff3A5051)),
                      )
                    ],
                  ),
                ),
                InkWell(
                  splashColor: Colors.transparent,
                  hoverColor: Colors.transparent,
                  focusColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  onTap: () {
                    setState(() {
                      pageIndex = 1;
                    });
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'ACHIEVEMENTS',
                        style: TextStyle(
                            fontSize: 16,
                            color: pageIndex == 1
                                ? Colors.white
                                : Color(0xff3A5051),
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                        height: 4,
                        width: MediaQuery.of(context).size.width / 2,
                        decoration: BoxDecoration(
                            color: pageIndex == 1
                                ? Color(0xff95CBCF)
                                : Color(0xff3A5051)),
                      )
                    ],
                  ),
                ),
              ],
            ),
            Expanded(
              child: pageIndex == 0 ? BarChartSample2() : Column(),
            ),
            SizedBox(
              height: 50,
            )
          ],
        ),
      ),
    );
  }
}
