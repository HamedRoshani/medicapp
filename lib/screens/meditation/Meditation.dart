import 'dart:async';

import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/material.dart';

import 'package:flutter_svg/flutter_svg.dart';

class MeditationScreen extends StatefulWidget {
  const MeditationScreen({Key? key}) : super(key: key);

  @override
  State<MeditationScreen> createState() => _MeditationScreenState();
}

class _MeditationScreenState extends State<MeditationScreen> {
  final CustomTimerController _controller = CustomTimerController();
  @override
  void initState() {
    _controller.start();
    super.initState();
  }

  bool start = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff253334),
      body: Scaffold(
        backgroundColor: Color(0xff253334),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset('assets/Home/MenuIcon.svg'),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: SvgPicture.asset(
                        'assets/Home/Logo.svg',
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        image: const DecorationImage(
                            image: AssetImage('assets/Home/Avatar.JPG'),
                            fit: BoxFit.cover),
                        // color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                Text(
                  'Meditation',
                  style: TextStyle(
                      fontSize: 45,
                      fontWeight: FontWeight.w400,
                      color: Colors.white),
                ),
                Text(
                  'Guided by a short introductory course start trying meditation.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: '',
                    color: Colors.white30,
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Image.asset(
                  'assets/Home/meditating.png',
                  width: 283,
                ),
                SizedBox(
                  height: 30,
                ),
                CustomTimer(
                  controller: _controller,
                  begin: Duration(minutes: 45),
                  end: Duration(),
                  builder: (time) {
                    return Text(
                      "${time.minutes}:${time.seconds}",
                      style: TextStyle(fontSize: 24.0),
                    );
                  },
                ),
                SizedBox(
                  height: 30,
                ),
                SizedBox(
                  width: 186,
                  height: 61,
                  child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          if (start) {
                            _controller.pause();
                            start = false;
                          } else {
                            _controller.start();
                            start = true;
                          }
                        });
                      },
                      child: start
                          ? Text(
                              'Stop',
                              style: TextStyle(fontSize: 35),
                            )
                          : Text(
                              'Start',
                              style: TextStyle(fontSize: 35),
                            )),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
