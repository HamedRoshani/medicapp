import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:medicapp/screens/Home/Home.dart';

class Authscreen extends StatelessWidget {
  const Authscreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: Image.asset(
              'assets/splash/Splash.png',
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset('assets/splash/Logo.svg'),
                SizedBox(
                  height: 12,
                ),
                SizedBox(
                  height: 140,
                  width: 260,
                  child: Column(
                    children: [
                      Text(
                        'WELCOME',
                        style: themeData.textTheme.subtitle1,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Text(
                        'Do meditation. Stay focused.Live a healthy life.',
                        style: themeData.textTheme.bodyText1,
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                ElevatedButton(
                  style: ButtonStyle(
                    minimumSize: MaterialStateProperty.all(
                      Size(321, 61),
                    ),
                  ),
                  onPressed: () {
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ));
                  },
                  child: Text(
                    'Login With Email',
                    style:
                        themeData.textTheme.bodyText1!.copyWith(fontSize: 25),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Don’t have an account?',
                        style: themeData.textTheme.bodyText1!
                            .copyWith(fontSize: 15)),
                    TextButton(
                        onPressed: () {},
                        child: Text(
                          'Sign Up',
                          style: themeData.textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.bold, fontSize: 15),
                        ))
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          Positioned.fill(
            child: Image.asset(
              'assets/Login&signup/Loginfull.png',
              fit: BoxFit.cover,
            ),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(35, 101, 35, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SvgPicture.asset(
                    'assets/splash/Logo.svg',
                    width: 44,
                  ),
                  SizedBox(
                    height: 31,
                  ),
                  Text(
                    'Sign In',
                    style: themeData.textTheme.bodyText1!
                        .copyWith(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    'Sign in now to acces your excercises and saved music.',
                    style: themeData.textTheme.bodyText2,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffBEC2C2))),
                        label: Text('Email Address'),
                        labelStyle: themeData.textTheme.bodyText2!
                            .copyWith(fontSize: 15)),
                    style: themeData.textTheme.bodyText2,
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffBEC2C2))),
                        label: Text('Password'),
                        labelStyle: themeData.textTheme.bodyText2!
                            .copyWith(fontSize: 15)),
                    style: themeData.textTheme.bodyText2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {},
                        child: Text(
                          'Forgot Password?',
                          style: themeData.textTheme.bodyText2!
                              .copyWith(fontSize: 15),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      minimumSize: MaterialStateProperty.all(
                        Size(321, 61),
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Homescreen(),
                          ));
                    },
                    child: Text(
                      'Login',
                      style:
                          themeData.textTheme.bodyText1!.copyWith(fontSize: 25),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('Don’t have an account?',
                          style: themeData.textTheme.bodyText1!
                              .copyWith(fontSize: 15)),
                      const SizedBox(
                        width: 6,
                      ),
                      TextButton(
                          onPressed: () {},
                          child: Text(
                            'Sign Up',
                            style: themeData.textTheme.bodyText1!.copyWith(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          )),
                    ],
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
