import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PlayScreen extends StatefulWidget {
  const PlayScreen({Key? key}) : super(key: key);

  @override
  State<PlayScreen> createState() => _PlayScreenState();
}

bool isplayed = true;

class _PlayScreenState extends State<PlayScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff253334),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset('assets/Home/MenuIcon.svg'),
                  Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: SvgPicture.asset(
                      'assets/Home/Logo.svg',
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      image: const DecorationImage(
                          image: AssetImage('assets/Home/Avatar.JPG'),
                          fit: BoxFit.cover),
                      // color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              const Center(
                child: CircleAvatar(
                  radius: 150,
                  backgroundImage: AssetImage('assets/Home/Playscreen.png'),
                ),
              ),
              const SizedBox(
                height: 17,
              ),
              Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      'Painting Forest',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontFamily: '',
                          fontSize: 28),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    const Text(
                      'By: Painting with Passion',
                      style: TextStyle(
                        fontFamily: '',
                        color: Colors.white38,
                      ),
                    ),
                    SizedBox(
                      height: 70,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            CupertinoIcons.shuffle,
                            color: Colors.white,
                          ),
                        ),
                        IconButton(
                          onPressed: (() {}),
                          icon: Icon(
                            CupertinoIcons.backward_end_fill,
                            color: Colors.white,
                          ),
                        ),
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 40,
                          child: IconButton(
                            onPressed: () {
                              setState(() {
                                isplayed = !isplayed;
                              });
                            },
                            icon: isplayed
                                ? Icon(
                                    CupertinoIcons.play_fill,
                                    color: Colors.black,
                                    size: 35,
                                  )
                                : Icon(
                                    CupertinoIcons.pause_fill,
                                    color: Colors.black,
                                    size: 35,
                                  ),
                          ),
                        ),
                        IconButton(
                          onPressed: (() {}),
                          icon: Icon(
                            CupertinoIcons.forward_end_fill,
                            color: Colors.white,
                          ),
                        ),
                        IconButton(
                          onPressed: (() {}),
                          icon: Icon(
                            CupertinoIcons.repeat,
                            color: Colors.white,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
