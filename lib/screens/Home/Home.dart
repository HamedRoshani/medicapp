import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Homescreen extends StatelessWidget {
  const Homescreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ThemeData themedata = Theme.of(context);
    return Scaffold(
      backgroundColor: Color(0xff253334),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 50,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset('assets/Home/MenuIcon.svg'),
                  Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: SvgPicture.asset(
                      'assets/Home/Logo.svg',
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      image: const DecorationImage(
                          image: AssetImage('assets/Home/Avatar.JPG'),
                          fit: BoxFit.cover),
                      // color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 25,
              ),
              Text(
                'Welcome back, Afreen!',
                style: themedata.textTheme.bodyText1!.copyWith(fontSize: 30),
              ),
              const Text('How are you feeling today ?'),
              const SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 65,
                        height: 68,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              'assets/Home/Calm.svg',
                              width: 45,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      const Text(
                        'Calm',
                        style: TextStyle(
                            fontSize: 15, color: Colors.white, fontFamily: ""),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 65,
                        height: 68,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              'assets/Home/Relax.svg',
                              width: 45,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      const Text(
                        'Relax',
                        style: TextStyle(
                            fontSize: 15, color: Colors.white, fontFamily: ""),
                      )
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 65,
                        height: 68,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              'assets/Home/Focus.svg',
                              width: 45,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      const Text(
                        'Focus',
                        style: TextStyle(
                            fontSize: 15, color: Colors.white, fontFamily: ""),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 65,
                        height: 68,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              'assets/Home/Spiral.png',
                              width: 45,
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      const Text(
                        'Anxious',
                        style: TextStyle(
                            fontSize: 15, color: Colors.white, fontFamily: ""),
                      ),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Stack(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    width: 339,
                    height: 170,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 40, top: 20),
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              'Meditation 101',
                              style: TextStyle(
                                  color: Color(0xff253334),
                                  fontWeight: FontWeight.bold,
                                  fontFamily: '',
                                  fontSize: 22),
                            ),
                            const SizedBox(
                              height: 4,
                            ),
                            const SizedBox(
                              width: 156,
                              child: Text(
                                'Techniques,Benefits,and a Beginner’s How-To',
                                style: TextStyle(
                                    fontFamily: 'Alegreya',
                                    color: Colors.black,
                                    fontSize: 16),
                              ),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              width: 138,
                              height: 39,
                              decoration: BoxDecoration(
                                color: Color(0xff253334),
                                borderRadius: BorderRadius.circular(12),
                              ),
                              child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    Text(
                                      'watch now',
                                      style: TextStyle(
                                          fontFamily: '',
                                          fontSize: 15,
                                          color: Colors.white),
                                    ),
                                    SizedBox(
                                      width: 6,
                                    ),
                                    CircleAvatar(
                                        radius: 10,
                                        backgroundColor: Colors.white,
                                        child: Center(
                                          child: Icon(
                                            CupertinoIcons.play_fill,
                                            color: Colors.black,
                                            size: 15,
                                          ),
                                        ))
                                  ]),
                            ),
                          ],
                        ),
                        Expanded(
                          child: Image.asset('assets/Home/HomePost1.png'),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 18,
              ),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                width: 339,
                height: 170,
                child: Padding(
                  padding: const EdgeInsets.only(left: 40, top: 20),
                  child: Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Cardio Meditation',
                            style: TextStyle(
                                color: Color(0xff253334),
                                fontWeight: FontWeight.bold,
                                fontFamily: '',
                                fontSize: 22),
                          ),
                          const SizedBox(
                            height: 4,
                          ),
                          const SizedBox(
                            width: 173,
                            child: Text(
                              'Basics of Yoga for Beginners or Experienced Professionals',
                              style: TextStyle(
                                  fontFamily: 'Alegreya',
                                  color: Colors.black,
                                  fontSize: 16),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Container(
                            width: 138,
                            height: 39,
                            decoration: BoxDecoration(
                              color: Color(0xff253334),
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: const [
                                Text(
                                  'watch now',
                                  style: TextStyle(
                                      fontFamily: '',
                                      fontSize: 15,
                                      color: Colors.white),
                                ),
                                SizedBox(
                                  width: 6,
                                ),
                                CircleAvatar(
                                  radius: 10,
                                  backgroundColor: Colors.white,
                                  child: Center(
                                    child: Icon(
                                      CupertinoIcons.play_fill,
                                      color: Colors.black,
                                      size: 15,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Image.asset('assets/Home/HomePost2.png'),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
