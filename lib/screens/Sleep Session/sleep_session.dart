import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Sleep_session extends StatelessWidget {
  const Sleep_session({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff253334),
      body: Scaffold(
        backgroundColor: const Color(0xff253334),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(25, 0, 25, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 50,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SvgPicture.asset('assets/Home/MenuIcon.svg'),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: SvgPicture.asset(
                        'assets/Home/Logo.svg',
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 40,
                      decoration: BoxDecoration(
                        image: const DecorationImage(
                            image: AssetImage('assets/Home/Avatar.JPG'),
                            fit: BoxFit.cover),
                        // color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 60,
                ),
                Text(
                  'Sleep Session',
                  style: TextStyle(
                      fontSize: 30,
                      color: Colors.white,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 100,
                      height: 163,
                      decoration: BoxDecoration(
                        color: Color(0xff69B09C),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Column(
                        children: [
                          Text(
                            '5h 30m',
                            style: TextStyle(
                                color: Colors.white, fontFamily: ''),
                          ),
                          Text(
                            'Sleep',
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                                fontFamily: ''),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 100,
                      height: 163,
                      decoration: BoxDecoration(
                          color: Color(0xff498A78),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                    Container(
                      width: 100,
                      height: 163,
                      decoration: BoxDecoration(
                          color: Color(0xff69B09C),
                          borderRadius: BorderRadius.circular(20)),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
