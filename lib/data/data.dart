import 'package:flutter/cupertino.dart';

class ListItems {
  final String name;
  final String time;
  final String image;
  final int listeningCount;

  ListItems(
      {required this.name,
      required this.time,
      required this.image,
      required this.listeningCount});
}

class AppData {
  static List<ListItems> get items {
    return [
      ListItems(
          name: 'Painting Forest',
          time: '20 Min',
          image: 'assets/Home/List1.png',
          listeningCount: 59999),
      ListItems(
          name: 'Painting Forest',
          time: '20 Min',
          image: 'assets/Home/List2.png',
          listeningCount: 59999),
      ListItems(
          name: 'Painting Forest',
          time: '20 Min',
          image: 'assets/Home/List3.png',
          listeningCount: 59999),
      ListItems(
          name: 'Painting Forest',
          time: '20 Min',
          image: 'assets/Home/List4.png',
          listeningCount: 59999),
    ];
  }
}
