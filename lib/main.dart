import 'package:flutter/material.dart';

import 'package:medicapp/data/data.dart';
import 'package:medicapp/screens/Home/Home.dart';

import 'package:medicapp/screens/Login&signup/Splash.dart';
import 'package:medicapp/screens/Login&signup/Splash.dart';
import 'package:medicapp/screens/MusicList/MusicList.dart';
import 'package:medicapp/screens/Play/Play.dart';
import 'package:medicapp/screens/Profile/profile.dart';
import 'package:medicapp/screens/Sleep%20Session/sleep_session.dart';
import 'package:medicapp/screens/meditation/Meditation.dart';
import 'package:medicapp/screens/toolsPage/tools.dart';
import 'package:medicapp/test/chart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final defualtTextStyle =
      const TextStyle(color: Colors.black, fontFamily: 'Alegreya');
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final data = AppData.items;
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Alegreya',
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(
              Color(0xff7C9A92),
            ),
          ),
        ),
        textTheme: const TextTheme(
          subtitle1: TextStyle(
            fontFamily: 'Alegreya',
            fontWeight: FontWeight.bold,
            color: Colors.white,
            fontSize: 36,
          ),
          bodyText1: TextStyle(
            // fontFamily: "Alegreya",
            fontWeight: FontWeight.w400,
            fontSize: 20,
            color: Colors.white,
          ),
          bodyText2: TextStyle(
            fontSize: 20,
            color: Color(0xffC4C4C4),
          ),
        ),
        primarySwatch: Colors.blue,
      ),
      home: Sleep_session(),
      debugShowCheckedModeBanner: false,
    );
  }
}
